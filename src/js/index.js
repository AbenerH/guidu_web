const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const morgan = require('morgan');
const mysql = require('mysql');
var jsdom = require('jsdom');
const { JSDOM } = jsdom;
const { window } = new JSDOM();
const { document } = (new JSDOM('')).window;
global.document = document;
var $ = require('jquery')(window);
const myConnection = require('express-myconnection');
const body_parser = require('body-parser');

var app = express();

//importing routes
const informationRoute = require('../routes/information.js');

app.use(body_parser.json());

//Settings

app.set('port', process.env.PORT || 3000);
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, '../views'))

//middlewares
app.use(morgan('dev'));
app.use(myConnection(mysql, {
    host: 'localhost',
    user: 'root',
    password: '1234',
    port: '3307',    
    database: 'web_information'
}, 'single'));

/*var mysqlConnection = mysql.createConnection({
    port: '3307',
    host: 'localhost',
    user: 'root',
    password: '1234',
    database: 'web_information' 
});

mysqlConnection.connect((err)=>{
    if(!err)
    console.log('Connection to Database Successful!');
    else
    console.log('Database Connection Error /n Error: ' + JSON.stringify(err, undefined, 2));
});

*/

//routes
app.use('/', informationRoute);

//static files
app.use(express.static(path.join(__dirname, '../public')));
app.use(favicon(path.join(__dirname, '../public/resources/logo.jpg'))); 

//To start the server
app.listen(app.get('port'), ()=>{
    console.log('Express Server is running at port No: 3000')    
});