const express = require('express');
const router = express.Router();

const infoController = require("../controllers/informationController.js")

router.get('/index.html', infoController.show); 

module.exports = router;