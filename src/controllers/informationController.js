const controller = {};

controller.show = (req, res) =>{
    req.getConnection((err, conn) =>{
        conn.query('Select * FROM service', (err, services) =>{
            if(err){
                res.json(err);
            }
            console.log(services);
            res.render('services', {
                data: services
            });
        });
    });
};

module.exports = controller;